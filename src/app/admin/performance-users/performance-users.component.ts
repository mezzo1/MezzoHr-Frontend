import { Component, OnInit } from '@angular/core';
import { PerformanceService } from 'src/app/Service/performance.service';
import { UserService } from 'src/app/Service/user.service';

@Component({
  selector: 'app-performance-users',
  templateUrl: './performance-users.component.html',
  styleUrls: ['./performance-users.component.css']
})
export class PerformanceUsersComponent implements OnInit {

  constructor(private us: UserService,private ps : PerformanceService) { }
  userData : any =[];
  userSelected : number = 0;
  YearSelected: number = 0;
  ListYear : any =[];
  ListPerformance: any =[];
  Performance: any =[];
  ngOnInit(): void {
    this.refreshUsers()
  }
  //get All users
  refreshUsers(){
    this.us.getAll().subscribe(res => {
      this.userData = res;

      });

  }

  userChanged(i :EventTarget | null){
    this.ListYear = [];
    this.ps.get(this.userSelected).subscribe((res : any)=>{
      this.ListPerformance = res;
      this.laodPerf()
      res.forEach((element: { performanceYear: any; }) => {
        this.ListYear.push(element.performanceYear)
      });
    })
  }

  laodPerf(){
    this.Performance = [];
    const p = {
      "nbrDayC": 0,
      "nbrHHC": 0,
      "tauxAbsences": 0
  }
     this.ListPerformance.forEach((element: { nbrDayC: number; nbrHHC: number; tauxAbsences: number; }) => {
        p.nbrDayC = p.nbrDayC + element.nbrDayC;
        p.nbrHHC = p.nbrHHC + element.nbrHHC;
        p.tauxAbsences = p.tauxAbsences + element.tauxAbsences;
    });
    console.log(p);
    this.Performance = p;
  }

  YearChanged(i :EventTarget | null){
    this.Performance = [];
    this.ps.getPerfYear(this.userSelected,this.YearSelected).subscribe((res : any)=>{
      this.Performance = res;
    })
  }
}

