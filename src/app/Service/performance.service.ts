import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const  APIUrlUser ="http://localhost:8080/Performance";

@Injectable({
  providedIn: 'root'
})

export class PerformanceService {
  constructor(private http:HttpClient) { }
  //Crud User
  get(id: any): Observable<any> {
    return this.http.get(`${APIUrlUser}/${id}`);
  }
  getPerfYear(id: any, year : any): Observable<any> {
    return this.http.get<any[]>(APIUrlUser+"/"+id+"/"+year);
  }

}
